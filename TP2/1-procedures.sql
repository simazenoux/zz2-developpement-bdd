-- 1
CREATE or REPLACE PROCEDURE createdept_mazenoux (numero_dept IN dept.deptno%type, dept_name IN dept.dname%type, localisation IN dept.loc%type)
IS 
num_dept dept.deptno%type ;    
BEGIN
    SELECT deptno INTO num_dept FROM dept WHERE deptno = numero_dept;

    EXCEPTION WHEN NO_DATA_FOUND THEN
        INSERT INTO dept VALUES( numero_dept, dept_name, localisation) ;
END createdept_mazenoux;
/

EXEC createdept_mazenoux (40 , 'IT' , 'WASHINGTON')


-- 2
create table SalIntervalle_mazenoux (job varchar2(9), lsal number(7,2), hsal number(7,2));
insert into SalIntervalle_mazenoux values ('ANALYST', 2500, 3000) ;
insert into SalIntervalle_mazenoux values ('CLERK', 900, 1300) ;
insert into SalIntervalle_mazenoux values ('MANAGER', 2400, 3000) ;
insert into SalIntervalle_mazenoux values ('PRESIDENT', 4500, 4900) ;
insert into SalIntervalle_mazenoux values ('SALESMAN', 1200, 1700) ;

select * from SalIntervalle_mazenoux;

CREATE or REPLACE FUNCTION salok_mazenoux (sjob IN emp.job%type, salaire IN emp.sal%type) return number
IS 
lowerSal SalIntervalle_mazenoux.lsal%type;
higherSal SalIntervalle_mazenoux.hsal%type;
res number := 0;
BEGIN
    SELECT lsal, hsal INTO lowerSal, higherSal FROM SalIntervalle_mazenoux WHERE job = sjob;

    IF lowerSal <= salaire AND salaire <= higherSal THEN
		res := 1;
	END IF;
	RETURN res;
END salok_mazenoux;
/
	

variable t number;
exec :t:=salok_mazenoux('ANALYST', 2900);
print t;
exec :t:=salok_mazenoux('ANALYST', 3900);
print t;



-- 3
CREATE or REPLACE PROCEDURE raisesalary_mazenoux(emp_id IN emp.EMPNO%type, amount IN emp.SAL%type)
IS 
res number;
sjob emp.JOB%type ;
salary emp.SAL%type;
BEGIN
	SELECT job, sal INTO sjob, salary FROM emp WHERE empno = emp_id;
	res:=salok_mazenoux(sjob, salary + amount);
	IF res = 1 THEN
		UPDATE emp SET SAL = salary + amount WHERE empno = emp_id;
	ELSE
		DBMS_OUTPUT.PUT_LINE('Augmentation impossible');
	END IF;
END raisesalary_mazenoux;
/
EXEC raisesalary_mazenoux (7000, 100)



