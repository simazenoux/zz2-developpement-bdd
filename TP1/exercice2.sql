DECLARE
    chaine VARCHAR2(20);
BEGIN
    FOR I IN 1 .. 30
    LOOP
        IF MOD(I, 2) = 0 THEN
            chaine := CONCAT(I, ' EST PAIR');
        ELSE
            chaine := CONCAT(I, ' EST IMPAIR');
        END IF;
        INSERT INTO temp (num_col1, num_col2, char_col) VALUES(I, I*100, chaine);
    END LOOP;
END;
/