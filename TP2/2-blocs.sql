

DECLARE
table_utilisateur USER_TABLES.table_name%type;
res NUMBER;
CURSOR c1 IS SELECT table_name FROM user_tables ORDER BY table_name DESC;
BEGIN
	OPEN c1;
	LOOP
		FETCH c1 INTO table_utilisateur;
		EXIT WHEN (c1%NOTFOUND);
		SELECT INSTR(table_utilisateur, '_OLD') INTO res FROM DUAL;
		IF (res > 0) THEN
			EXECUTE IMMEDIATE 'DROP_TABLE ' || table_utilisateur;
		END IF;

		EXECUTE IMMEDIATE 'CREATE TABLE ' || table_utilisateur || '_OLD AS SELECT * FROM' || table_utilisateur;
		COMMIT;
	END LOOP;
	CLOSE c1;
END;
/

SELECT table_name FROM user_tables order by table_name desc;