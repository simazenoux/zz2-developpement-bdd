CREATE OR REPLACE TRIGGER log_pilote 
BEFORE DELETE OR UPDATE OF * ON pilote
FOR EACH ROW
ope VARCHAR2(40)
BEGIN
    IF updating THEN
        ope := 'update';
    ELSE
        ope := 'delete';
    END IF;

    INSERT INTO Audit_pilote VALUES (sysdate, user, ope, :old.)
END;
/