DROP TABLE AUTEURS;
DROP TABLE OUVRAGES;

-- Creation des tables

CREATE TABLE AUTEURS
(
	num         NUMBER(2), 
	nom         VARCHAR2(14), 
    prenom      VARCHAR2(14),
	pays        CHAR(2),
    tel         NUMBER(10)
);


CREATE TABLE OUVRAGES
(
    code        NUMBER(3),
    tire        VARCHAR2(30),
    mgr         NUMBER(4)
);


-- Insertion des lignes de la table ouvrage 

INSERT INTO OUVRAGES(code, titre, prix) VALUES (001, "Intro aux BD", 260);
INSERT INTO OUVRAGES(code, titre)       VALUES (002, "Journal de Bolivie");
INSERT INTO OUVRAGES(code, titre)       VALUES (003, "L'homme aux sandales");
