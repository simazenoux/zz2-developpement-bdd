DECLARE
BEGIN
    INSERT INTO temp (num_col1, num_col2, char_col) 
    SELECT sal, empno, ename FROM emp
    WHERE NVL(sal,0) + NVL(comm,0) > 2000;
END;
/