# ZZ2 - Développement des bases de données

## Connexion au serveur Oracle

Si vous n'êtes pas sur le même réseau que le serveur, il est nécessaire de se connecter au VPN de l'ISIMA (<https://doc.isima.fr/vpn/presentation/>) :  
``` sudo openvpn ~/.vpn/vpn-student-tcp.ovpn ```

Il faut d'abord se connecter via ssh à la machine oracle-18c (le mot de passe est le même que le nom de l'utilisateur sélectionné) :  
``` ssh [user01,user60]@oracle-18c ```

Une fois sur la machine oracle-18c, on se connection à la base de données orable via la commande suivante :  
``` sqlplus scott/tiger ```

## DBMS

Pour activer les logs sur le server, il est nécessaire de lancer sous sql la commande suivante :
``` SET SERVEROUTPUT ON; ```

## SQL Loader

Pour accéder au programme SQL Loader :  
``` $ORACLE_HOME/bin/sqlldr ```

Pour ajouter un fichier de controle :  
``` $ORACLE_HOME/bin/sqlldr login fichier.ctl ```

## Consignes compte rendu

Code et solution pour chaque question, à rendre le 25 mars 2022 sur l'ent au plus tard
