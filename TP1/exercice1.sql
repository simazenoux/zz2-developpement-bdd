DECLARE
    nom VARCHAR2(100);
    salaire INTEGER;
    commission INTEGER;
BEGIN
    SELECT ename, sal, comm INTO nom, salaire, commission FROM emp
    WHERE ename='MILLER';

    DBMS_OUTPUT.PUT_LINE(nom || ' ' || salaire || ' ' || commission);
END;
/