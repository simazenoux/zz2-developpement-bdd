DECLARE
v_empno emp.empno%type;
v_mgr emp.mgr%type;
v_sal emp.sal%type;
found BOOLEAN;
BEGIN
    found := FALSE;

    v_empno := 7902;
    SELECT mgr INTO v_mgr FROM emp WHERE empno = v_empno;

    WHILE v_mgr IS NOT NULL AND found = FALSE
    LOOP
        v_empno := v_mgr;

        SELECT sal INTO v_sal FROM emp WHERE empno = v_empno;
        IF v_sal >= 4000 THEN
            found := TRUE;
            INSERT INTO temp (num_col1, char_col)
            SELECT sal, ename FROM emp 
            WHERE empno = v_empno;
        END IF;

        SELECT mgr INTO v_mgr FROM emp WHERE empno = v_empno;
    END LOOP;
END;
/