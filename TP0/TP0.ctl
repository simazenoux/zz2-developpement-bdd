LOAD DATA INFILE *
REPLACE INTO TABLE Auteurs
fields terminated ","
(Num, Nom, Prenom nullif Prenom="n", Pays nullif Pays="n", Tel nullif Tel="n")
BEGINDATA
1,Dupont,Jacques,FR,0473151585
2,Durand,Marie,GB,n
3,Dupont,Pierre,n,n
4,Dupont,n,n,n