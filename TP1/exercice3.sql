DECLARE
BEGIN
    INSERT INTO temp (num_col1, num_col2, char_col) 
    SELECT sal, empno, ename FROM emp
    ORDER BY sal DESC
    FETCH FIRST 5 ROWS ONLY; 
END;
/