-- 1

CREATE OR REPLACE TRIGGER raise_salary_mazenoux
BEFORE UPDATE ON emp FOR EACH ROW
WHEN (new.sal < old.sal)
BEGIN
    RAISE_APPLICATION_ERROR(-20000, "Le salaire d'un employé ne diminue jamais");
END;
/


-- 2

CREATE OR REPLACE TRIGGER numdept_mazenoux
BEFORE INSERT OR UPDATE ON dept FOR EACH ROW
WHEN (new.deptno <= 61 OR 69 <= new.deptno)
BEGIN
    RAISE_APPLICATION_ERROR(-20000, "Les départements doivent être compris entre 61 et 69")
END;
/

INSERT INTO dept (deptno) VALUES 69;
INSERT INTO dept (deptno) VALUES 70;


-- 3

CREATE OR REPLACE TRIGGER dept_mazenoux
BEFORE INSERT OR UPDATE ON emp FOR EACH ROW
BEGIN

    IF (SELECT COUNT(deptno) FROM dept WHERE deptno= :new.deptno) = 0 THEN
        INSERT INTO dept VALUES (:new.deptno, 'A_SAISIR', 'A_SAISIR');
    END IF;
END;
/

DELETE FROM dept WHERE deptno = 69;
INSERT INTO dept (deptno) VALUES 

-- 4

CREATE OR REPLACE TRIGGER noweek_mazenoux
BEFORE UPDATE ON emp FOR EACH ROW
BEGIN
    IF to_char(sysdate, 'DY') IN ('FRI', 'SUN') THEN
        RAISE_APPLICATION_ERROR(-20000, "La table employé ne peut être modifié le week-end");
    END IF;
END;
/

UPDATE emp SET sal = 100000 WHERE empid = 7000;

-- 5

ALTER TRIGGER noweek_mazenoux DISABLE;

UPDATE emp SET sal = 100000 WHERE empno = 7000;

-- 6

ALTER TRIGGER noweek_mazenoux ENABLE;

-- 7

CREATE TABLE stats_mazenoux(
    TypeMaj CHAR(6),
    NbMaj NUMBER(8),
    Date_derniere_Maj date
);
INSERT INTO stats_mazenoux (TypeMaj, NbMaj) VALUES('INSERT', 0);
INSERT INTO stats_mazenoux (TypeMaj, NbMaj) VALUES('UPDATE', 0);
INSERT INTO stats_mazenoux (TypeMaj, NbMaj) VALUES('DELETE', 0);

-- A

CREATE OR REPLACE TRIGGER stat_mazenoux
AFTER INSERT OR UPDATE OR DELETE ON emp 
FOR EACH ROW
DECLARE
    typeOfMaj CHAR(6);
BEGIN
    IF INSERTING THEN
        typeOfMaj = 'INSERT';
    ELSIF UPDATING THEN
        typeOfMaj = 'UPDATE';
    ELSE
        typeOfMaj = 'DELETE';
    END IF;
    
    UPDATE stats_mazenoux SET NbMaj = NbMaj + 1, Date_derniere_Maj = sysdate
    WHERE typeMaj = typeOfMaj;
END;
/

insert INTO emp values (7001,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
insert INTO emp values (7002,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
insert INTO emp values (7003,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
update emp set ename = "MAZE";
update emp set ename = "NOUX"

SELECT * FROM stats_mazenoux;

delete from emp where empno between 7001 AND 7003;
UPDATE stats_mazenoux SET NbMaj = 0, Date_derniere_Maj = NULL;

-- B

CREATE OR REPLACE TRIGGER stat_mazenoux
AFTER INSERT OR UPDATE OR DELETE ON emp
DECLARE
    typeOfMaj CHAR(6);
BEGIN
    IF INSERTING THEN
        typeOfMaj = 'INSERT';
    ELSIF UPDATING THEN
        typeOfMaj = 'UPDATE';
    ELSE
        typeOfMaj = 'DELETE';
    END IF;
    
    UPDATE stats_mazenoux SET NbMaj = NbMaj + 1, Date_derniere_Maj = sysdate
    WHERE typeMaj = typeOfMaj;
END;
/


insert INTO emp values (7001,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
insert INTO emp values (7002,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
insert INTO emp values (7003,'MAZENOUX','SALESMAN',7566,to_date('17-12-1980','dd-mm-yyyy'),2200,NULL,20) ; 
update emp set ename = "MAZE";
update emp set ename = "NOUX"
delete from emp where empno between 7001 AND 7003;

SELECT * FROM stats_mazenoux;

UPDATE stats_mazenoux SET NbMaj = 0, Date_derniere_Maj = NULL;


-- 8

CREATE TABLE SalIntervalle_mazenoux(
    job  emp.job%type,
    lsal emp.sal%type,
    hsal emp.sal%type
);
INSERT INTO SalIntervalle_mazenoux VALUES('SALESMAN', 1000, 3000);
INSERT INTO SalIntervalle_mazenoux VALUES('MANAGER', 2000, 3000);
INSERT INTO SalIntervalle_mazenoux VALUES('CLERK', 1000, 2000);




CREATE TRIGGER checksal_mazenoux
BEFORE UPDATE ON empFOR EACH ROW 
WHEN (new.job != 'PRESIDENT')
DECLARE
    lowerSal emp.sal%type;
    higherSal emp.sal%type;
BEGIN
    SELECT lsal, hsal INTO lowerSal, higherSal FROM SalIntervalle_mazenoux
    WHERE job = :new.job;

    :new.sal = greatest(lowerSal, LEAST(higherSal, :old.sal + 100));
END;
/

SELECT sal, job, empno FROM emp WHERE empno = 7000;
UPDATE emp SET job='SALESMAN' WHERE empno=7000;
SELECT sal, job, empno FROM emp WHERE empno = 7000;
UPDATE emp SET job='MANAGER' WHERE empno=7000
SELECT sal, job, empno FROM emp WHERE empno = 7000;
UPDATE emp SET job='CLERK' WHERE empno=7000;
SELECT sal, job, empno FROM emp WHERE empno = 7000;
