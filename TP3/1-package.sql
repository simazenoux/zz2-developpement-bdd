CREATE OR REPLACE PACKAGE mazenoux AS
    TYPE emp_cursor IS RECORD ( emp_id NUMBER, nom VARCHAR2( 1 0 ) ) ;
    CURSOR emp_par_dep_mazenoux ( dep IN NUMBER) RETURN emp_cursor ;
    PROCEDURE raise_salary_mazenoux ( emp_id IN NUMBER, amount IN NUMBER) ;
    PROCEDURE afficher_emp_mazenoux ( deptno IN NUMBER) ;
END;
/


CREATE OR REPLACE PACKAGE BODY mazenoux AS
CURSOR emp_par_dep_mazenoux (dep IN NUMBER) RETURN emp_cursor IS
SELECT empno , ename FROM emp WHERE deptno = dep;



CREATE or REPLACE PROCEDURE raisesalary_mazenoux(emp_id IN emp.EMPNO%type, amount IN emp.SAL%type)
IS 
res number;
sjob emp.JOB%type ;
salary emp.SAL%type;
BEGIN
	SELECT job, sal INTO sjob, salary FROM emp WHERE empno = emp_id;
	res:=salok_mazenoux(sjob, salary + amount);
	IF res = 1 THEN
		UPDATE emp SET SAL = salary + amount WHERE empno = emp_id;
	ELSE
		DBMS_OUTPUT.PUT_LINE('Augmentation impossible');
	END IF;
END raisesalary_mazenoux;
/

PROCEDURE afficher_emp_mazenoux (deptno IN NUMBER)
IS
emp_id NUMBER;
nom VARCHAR2( 9 ) ;
BEGIN
    OPEN emp_par_dep_mazenoux ( deptno ) ;
    LOOP
        FETCH emp_par_dep_mazenoux INTO emp_id, nom ;
        EXIT WHEN(emp_par_dep_mazenoux%NOTFOUND) ;
        dbms_output.put_line( emp_id | | ' | ' | | nom );
    END LOOP;
    CLOSE emp_par_dep_mazenoux ;
END;
END;
/
exec mazenoux.afficher_emp_mazenoux( 20 ) ;